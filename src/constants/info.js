export const path = 'This represents the best path to convert the specified assets based on the order books on the network. ';
export const minimumReceived = 'This is the minimum amount of an asset you’ll receive after the swap';
export const slippageTolerance = 'This shows the minimum you’ll receive. Higher percentages mean you’ll receive less.';
