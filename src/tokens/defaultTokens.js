import XLM from './XLM';
import MOBI from './MOBI';
import REPO from './REPO';
import USD from './USD';
import CENTUS from './CENTUS';
import BTC from './BTC';
import ETH from './ETH';
import SLVR from './SLVR';
import GOLD from './GOLD';
import PALL from './PALL';
import LTC from './LTC';
import NGNT from './NGNT';
import CNY from './CNY';
import USDT from './USDT';
import AFR from './AFR';
import DEB from './DEB';
import USDC from './USDC';

export default [
  XLM,
  USDC,
  MOBI,
  USD,
  CENTUS,
  BTC,
  ETH,
  REPO,
  USDT,
  SLVR,
  GOLD,
  PALL,
  LTC,
  NGNT,
  CNY,
  AFR,
  DEB,
];
