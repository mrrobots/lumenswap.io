import logo from 'src/assets/images/repo.jpeg';

export default {
  code: 'REPO',
  logo,
  web: 'repocoin.io',
  issuer: 'GCZNF24HPMYTV6NOEHI7Q5RJFFUI23JKUKY3H3XTQAFBQIBOHD5OXG3B',
};
