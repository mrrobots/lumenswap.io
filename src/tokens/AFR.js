import logo from 'src/assets/images/afreum.png';

export default {
  code: 'AFR',
  logo,
  web: 'afreum.com',
  issuer: 'GBX6YI45VU7WNAAKA3RBFDR3I3UKNFHTJPQ5F6KOOKSGYIAM4TRQN54W',
};
