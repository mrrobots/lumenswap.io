import logo from 'src/assets/images/deb.png';

export default {
  code: 'DEB',
  logo,
  web: 'drivedeb.com',
  issuer: 'GB6G343Z2IHS3W2V6QGGEM622V5S4UUS7C2NEOZFPYG3DCEB256FAOZA',
};
