import logo from 'src/assets/images/centus.png';

export default {
  code: 'CENTUS',
  logo,
  web: 'centus.one',
  issuer: 'GAKMVPHBET4T7DPN32ODVSI4AA3YEZX2GHGNNSBGFNRQ6QEVKFO4MNDZ',
};
